How to serve webpage using NGINX in Ubuntu
1. Install nginx
2. Change configuration file(nginx.conf) in  /etc/nginx as follows:
```python
http{
    include mime.types;
    server {
            listen 80;
            root /path/to/folder/containing/index.html;

    }
}
events {}
```
3.  Start or reload nginx after changing configuaration file
```shell
systemctl start nginx
nginx -s reload
```
